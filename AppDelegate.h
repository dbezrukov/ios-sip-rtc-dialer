//
//  AppDelegate.h
//  IosSipRtcDialer
//
//  Created by dbezrukov on 2/11/14.
//
//

#import <UIKit/UIKit.h>

#import "SIPClient.h"

@protocol SIPPhoneApp<NSObject>

- (void)makeRegistration:(NSString *)domain user:(NSString *)user password:(NSString *)password;
- (void)makeCall:(NSString *)sipAddress;
- (void)hangUp;

@end

@class ViewController;

@interface AppDelegate : UIResponder<SIPClientDelegate,
                                    SIPPhoneApp,
                                    UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController *viewController;

@end
