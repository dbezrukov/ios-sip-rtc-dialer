#import <Foundation/Foundation.h>

@protocol SIPClientDelegate<NSObject>

- (void)onStatusUpdated:(NSString*)status;
- (void)onCallingStateChanged:(int)state;

@end

#import "RTCClient.h"

@interface SIPClient : NSObject<RTCClientDelegate>

- (id)initWithDelegate:(id<SIPClientDelegate>)delegate;
- (void)makeRegistration:(NSString*)domain user:(NSString*)user password:(NSString*)password;
- (void) makeCall:(NSString*)sipAddr;
- (void) answerCall;
- (void) callEstablished;
- (void) hangUp;
- (void) updateStatus:(NSString*)status;

@end