#import "SIPClient.h"

#import <dispatch/dispatch.h>
#include <ifaddrs.h>
#include <arpa/inet.h>

#import "DialInstance.hh"

#include "resip/dum/ClientInviteSession.hxx"
#include "resip/dum/ServerInviteSession.hxx"

using namespace resip;

enum STATE {
    IDLE = 1,
    INCOMING,
    OUTGOING,
    INCALL
};

NSString* offerSDP = NULL;
NSString* remoteSDP = NULL;

SIPClient* sipClient;

@interface SIPClient ()

@property(nonatomic, strong) RTCClient *rtcClient;
@property(nonatomic, assign) id<SIPClientDelegate> delegate;
@property(nonatomic, copy) NSString* sipAddress;
@property(nonatomic, assign) BOOL isPC;
@property(nonatomic, assign) BOOL initiator;
@property(nonatomic, assign) STATE currentstate;

@end

@implementation SIPClient

ClientRegistrationHandle reg_handle;
ClientInviteSessionHandle session_outgoing;
InviteSessionHandle session_incoming;

class MyRegistrationHandler :
public ClientRegistrationHandler
{
public:
	virtual void onSuccess(ClientRegistrationHandle h, const SipMessage& response) {
		NSLog(@"REGISTER success");
        [sipClient updateStatus: @"Registration successful"];
        reg_handle = h;
	};
    // Called when all of my bindings have been removed
	virtual void onRemoved(ClientRegistrationHandle, const SipMessage& response) { }
    /// call on Retry-After failure.
	/// return values: -1 = fail, 0 = retry immediately, N = retry in N seconds
	virtual int onRequestRetry(ClientRegistrationHandle, int retrySeconds, const SipMessage& response) {
		return retrySeconds;
	};
    /// Called if registration fails, usage will be destroyed (unless a
	/// Registration retry interval is enabled in the Profile)
	virtual void onFailure(ClientRegistrationHandle, const SipMessage& response) { NSLog(@"REGISTER failure"); };
};

class MyMessageHandler :
public InviteSessionHandler
{
public:
	/// called when an initial INVITE or the intial response to an outoing invite
	virtual void onNewSession(ClientInviteSessionHandle, InviteSession::OfferAnswerType oat, const SipMessage& msg) {
        NSLog(@"INVITE new session, ClientInviteSessionHandle");
    };
	virtual void onNewSession(ServerInviteSessionHandle h, InviteSession::OfferAnswerType oat, const SipMessage& msg) {
        NSLog(@"INVITE new session, ServerInviteSessionHandle");
    };
	/// Received a failure response from UAS
	virtual void onFailure(ClientInviteSessionHandle h, const SipMessage& msg) {
		h->end();
	};
    /// called when dialog enters the Early state - typically after getting 100
	virtual void onEarlyMedia(ClientInviteSessionHandle, const SipMessage&, const SdpContents&) { };
    /// called when dialog enters the Early state - typically after getting 18x
	virtual void onProvisional(ClientInviteSessionHandle, const SipMessage&) { };
    /// called when a dialog initiated as a UAC enters the connected state
	virtual void onConnected(ClientInviteSessionHandle h, const SipMessage& msg) {
        NSLog(@"INVITE connected");
        [sipClient updateStatus: @"Calling session established"];
        session_outgoing = h;
    };
    /// called when a dialog initiated as a UAS enters the connected state
	virtual void onConnected(InviteSessionHandle, const SipMessage& msg) {
    };
    virtual void onTerminated(InviteSessionHandle, InviteSessionHandler::TerminatedReason reason, const SipMessage* related=0) { };
    /// called when a fork that was created through a 1xx never receives a 2xx
	/// because another fork answered and this fork was canceled by a proxy.
	virtual void onForkDestroyed(ClientInviteSessionHandle) { };
    /// called when a 3xx with valid targets is encountered in an early dialog
	/// This is different then getting a 3xx in onTerminated, as another
	/// request will be attempted, so the DialogSet will not be destroyed.
	/// Basically an onTermintated that conveys more information.
	/// checking for 3xx respones in onTerminated will not work as there may
	/// be no valid targets.
	virtual void onRedirected(ClientInviteSessionHandle, const SipMessage& msg) {
        NSLog(@"INVITE redirected");
    };
    /// called when an SDP answer is received - has nothing to do with user
	/// answering the call
	virtual void onAnswer(InviteSessionHandle h, const SipMessage& msg, const SdpContents& sdp) {
        [sipClient updateStatus: @"Answer received"];
        NSLog(@"SIP onAnswer: %@", [NSString stringWithUTF8String:sdp.getBodyData().data()]);
        remoteSDP = [NSString stringWithUTF8String:sdp.getBodyData().data()];
        [sipClient callEstablished];
    };
    /// called when an SDP offer is received - must send an answer soon after this
	virtual void onOffer(InviteSessionHandle h, const SipMessage& msg, const SdpContents& sdp) {
        [sipClient updateStatus: @"Incoming call received"];
		session_incoming = h;
        NSLog(@"SIP onOffer: %@", [NSString stringWithUTF8String:sdp.getBodyData().data()]);
        remoteSDP = [NSString stringWithUTF8String:sdp.getBodyData().data()];
        [sipClient answerCall];
	};
    /// called when an Invite w/out SDP is sent, or any other context which
	/// requires an SDP offer from the user
	virtual void onOfferRequired(InviteSessionHandle, const SipMessage& msg) { };
	/// called if an offer in a UPDATE or re-INVITE was rejected - not real
	/// useful. A SipMessage is provided if one is available
	virtual void onOfferRejected(InviteSessionHandle, const SipMessage* msg) {
        NSLog(@"INVITE offer rejected");
    };
    /// called when INFO message is received
	virtual void onInfo(InviteSessionHandle, const SipMessage& msg) { };
    /// called when response to INFO message is received
	virtual void onInfoSuccess(InviteSessionHandle, const SipMessage& msg) { };
	virtual void onInfoFailure(InviteSessionHandle, const SipMessage& msg) { };
    /// called when MESSAGE message is received
	virtual void onMessage(InviteSessionHandle, const SipMessage& msg) { };
	/// called when response to MESSAGE message is received
	virtual void onMessageSuccess(InviteSessionHandle, const SipMessage& msg) { };
	virtual void onMessageFailure(InviteSessionHandle, const SipMessage& msg) { };
	/// called when an REFER message is received.  The refer is accepted or
	/// rejected using the server subscription. If the offer is accepted,
	/// DialogUsageManager::makeInviteSessionFromRefer can be used to create an
	/// InviteSession that will send notify messages using the ServerSubscription
	virtual void onRefer(InviteSessionHandle, ServerSubscriptionHandle, const SipMessage& msg) { };
	virtual void onReferNoSub(InviteSessionHandle, const SipMessage& msg) { };
	/// called when an REFER message receives a failure response
	virtual void onReferRejected(InviteSessionHandle, const SipMessage& msg) { };
	/// called when an REFER message receives an accepted response
	virtual void onReferAccepted(InviteSessionHandle, ClientSubscriptionHandle, const SipMessage& msg) { };
};

DialInstance* dialInstance;

- (id)initWithDelegate:(id<SIPClientDelegate>)delegate {
    [sipClient updateStatus:
        [NSString stringWithFormat:@"SIP stack initialized, ip is %@", [self getIPAddress]]];
    
    self = [super init];
    if (self) {
        sipClient = self;
        _delegate = delegate;
        
        self.currentstate = IDLE;
        self.rtcClient = [[RTCClient alloc] init];
        self.isPC = FALSE;
    }
    return self;
}

- (void)makeCall:(NSString*)destination {
    NSLog(@"SipClient::makeCall to %@", destination);
    self.sipAddress = destination;
    if (self.sipAddress.length > 0) {
        [self initiateCall];
    }
}

- (void)makeRegistration:(NSString*)domain user:(NSString*)user password:(NSString*)password
{
    NSLog(@"SipClient::registering user %@, password %@, domain %@", user, password, domain);
    
    MyRegistrationHandler* reg_handler = new MyRegistrationHandler();
    MyMessageHandler* msg_handler = new MyMessageHandler();
    
    dialInstance = new DialInstance(reg_handler, msg_handler);
    dialInstance->makeRegistration([domain UTF8String], [user UTF8String], [password UTF8String]);
}

- (void)setCurrentState:(STATE)state {
    self.currentstate = state;
    [self.delegate onCallingStateChanged: state];
}

- (int)getCurrentState {
    return self.currentstate;
}

/**
 * Make an outgoing call.
 */
- (void)initiateCall {
    [self updateStatus: [NSString stringWithFormat:@"Calling %@" , self.sipAddress]];
    [self askForOffer];
}

/**
 * Incoming call start.
 */
- (void)answerCall
{
    [self setCurrentState:INCOMING];
    [self askForAnswer:remoteSDP];
}

- (void)callEstablished
{
    [self setCurrentState:INCALL];
    
    NSLog(@"SIPCLient::callEstablished");
    
    [self updateStatus: @"Call established"];
    [self setCurrentState:INCALL];
    if (self.initiator) {
        [self.rtcClient receiveRemoteDescription:@"answer" desc:remoteSDP];
    }
}

- (void)updateStatus:(NSString*)status {
    [self.delegate onStatusUpdated: status];
}

- (void)hangUp {
    [self updateStatus: @"Hangup"];
    
    NSLog(@"Hangup 1/3 Closing peer connection");
    if(self.isPC) {
        self.isPC = FALSE;
        // close webrtc peer connection
        [self.rtcClient closePeerConnection];
    }
    
    NSLog(@"Hangup 2/3 Ending incoming call if any");
    // if call is incoming
    if (session_incoming.isValid()) {
        InviteSession* incoming = dynamic_cast<InviteSession*>(session_incoming.get());
        if (incoming) {
            incoming->end();
        }
    }
        
    NSLog(@"Hangup 3/3 Ending outgoing call if any");
    if (session_outgoing.isValid()) {
        // for outgoing call use session
        InviteSession* outgoing = dynamic_cast<InviteSession*>(session_outgoing.get());
        if (outgoing) {
            outgoing->end();
        }
    }
    
    // turn into idle state
    [self setCurrentState:IDLE];
}

// interaction with webRTC client

- (void)askForAnswer:(NSString*)sdp {
    NSLog(@"SIPCLient::askForAnswer");
    
    if (self.isPC) {
        [self.rtcClient receiveRemoteDescription:@"offer" desc:sdp];
    } else {
        self.initiator = FALSE;
        offerSDP = sdp;
        [self.rtcClient createPeerConnection:self initiator:self.initiator];
    }
}

- (void)askForOffer {
    NSLog(@"SipClient::askForOffer");
    
    if (self.isPC) {
        [self.rtcClient createOffer];
    } else {
        self.initiator = TRUE;
        [self.rtcClient createPeerConnection:self initiator:self.initiator];
    }
}

// RTCClientDelegate methods

- (void)onAnswer:(NSString*) SDPAnswer {
    NSLog(@"Answering the call");
    
    [sipClient updateStatus: @"Answering the call"];
    
    NSLog(@"Answering the call, SDPOffer %@", SDPAnswer);
    
    if (!session_incoming.isValid()) {
        [sipClient updateStatus: @"No session, returning"];
        NSLog(@"No session, returning");
        return;
    }
    
    Data *txt = new Data([SDPAnswer UTF8String]);
    
	HeaderFieldValue *hfv = new HeaderFieldValue(txt->data(), txt->size());
	Mime type("application", "sdp");
    
    SdpContents * contents = new SdpContents(hfv, type);
	session_incoming->provideAnswer( *contents );
    
    ServerInviteSessionHandle *server_h = (ServerInviteSessionHandle*)&session_incoming;
    server_h->get()->accept();
}

- (void)onOffer:(NSString*) SDPOffer {
    [self setCurrentState:OUTGOING];
    
    [sipClient updateStatus: @"Sending an offer"];
    
    NSLog(@"SIPClient::onOffer, address %@, offer %@", self.sipAddress, SDPOffer);
    dialInstance->makeInvite([self.sipAddress UTF8String], [SDPOffer UTF8String]);
}

- (void)onPeerConnectionCreated {
    NSLog(@"SIPClient::onPeerConnectionCreated");
    
    self.isPC = TRUE;
    
    if(!self.initiator) {
        [self askForAnswer:offerSDP];
    } else {
        [self askForOffer];
    }
}

- (void)onPeerConnectionEstablished {
}

- (void)dealloc {
    reg_handle->removeMyBindings();
	reg_handle->stopRegistering();
    
    dialInstance->shutdown();
}

- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}

@end
