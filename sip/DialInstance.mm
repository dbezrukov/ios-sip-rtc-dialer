#include "DialInstance.hh"

#include "resip/dum/DumShutdownHandler.hxx"
#include "resip/dum/ClientAuthManager.hxx"
#include "resip/dum/ClientInviteSession.hxx"
#include "resip/dum/ServerInviteSession.hxx"
#include "resip/dum/MasterProfile.hxx"
#include "resip/dum/AppDialogSetFactory.hxx"
#include "resip/dum/AppDialogSet.hxx"
#include "resip/dum/KeepAliveManager.hxx"

#include "rutil/Log.hxx"
#include "rutil/Logger.hxx"
#include "rutil/Subsystem.hxx"

#include <sstream>

using namespace resip;
using namespace std;

class MyShutdownHandler : public DumShutdownHandler
{
public:
    MyShutdownHandler():
    DumShutdownHandler(), done(false)
    {};
    
    virtual void onDumCanBeDeleted()
    {
        done = true;
    };
    bool done;
};

DialInstance::DialInstance(ClientRegistrationHandler* mRegHandler, InviteSessionHandler* mMsgHandler)
{
    // Stack initialization
    
    mSipStack = new resip::SipStack();
    
    mSipStack->addTransport(UDP, 5060);
    mSipStack->addTransport(TCP, 5060);
    
    mStackThread = new StackThread(*mSipStack);
    mStackThread->run();
    
    // DialogUsageManager initialization
    
    mDum = new DialogUsageManager(*mSipStack);
    
    mDumThread = new DumThread(*mDum);
    mDumThread->run();
    
    mDum->setClientRegistrationHandler(mRegHandler);
    mDum->setInviteSessionHandler(mMsgHandler);
}

void DialInstance::makeRegistration(const char* domain, const char* user, const char* password)
{
    // UserProfile initialization (Username, Passwort etc.)
    
    SharedPtr<MasterProfile> masterProfile = SharedPtr<MasterProfile>(new MasterProfile);
    
    std::stringstream ss;
    ss << "sip:" << user << "@" << domain;
    
    NameAddr default_from(ss.str().c_str());
    masterProfile->setDefaultFrom(default_from);
    masterProfile->setDigestCredential(domain, user, password);
    
    mDum->setMasterProfile(masterProfile);
    
    auto_ptr<ClientAuthManager> clientAuth(new ClientAuthManager);
    mDum->setClientAuthManager(clientAuth);
    
    // REGISTER
    
    SharedPtr<SipMessage> register_msg = mDum->makeRegistration(default_from);
    mDum->send(register_msg);
}

void DialInstance::makeInvite(const char* sipAddress, const char* sdp)
{
    SdpContents *sdp_content = build_sdp(sdp);
    NameAddr to(sipAddress);
    SharedPtr<SipMessage> invite_msg = mDum->makeInviteSession(to, sdp_content);
    mDum->send(invite_msg);
}

void DialInstance::shutdown()
{
    MyShutdownHandler shutdown;
    mDum->shutdown(&shutdown);
    while(!shutdown.done ) {
        // waiting
    }
 
    mDumThread->shutdown();
    mDumThread->join();

    mStackThread->shutdown();
    mStackThread->join();
}

SdpContents *DialInstance::build_sdp(const char* sdp)
{
    Data *txt = new Data(sdp);
    
    HeaderFieldValue *hfv = new HeaderFieldValue(txt->data(), txt->size());
    Mime type("application", "sdp");
    
    return new SdpContents(hfv, type);
}
