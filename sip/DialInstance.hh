
#ifndef __DIALINSTANCE_H
#define __DIALINSTANCE_H

#include "resip/dum/DialogUsageManager.hxx"
#include "resip/dum/ClientRegistration.hxx"
#include "resip/dum/InviteSessionHandler.hxx"
#include "resip/dum/RegistrationHandler.hxx"
#include "resip/dum/DumThread.hxx"

#include "resip/stack/Uri.hxx"
#include "resip/stack/Contents.hxx"
#include "resip/stack/SdpContents.hxx"
#include "resip/stack/SipStack.hxx"
#include "resip/stack/StackThread.hxx"

#include "rutil/Data.hxx"

class DialInstance {

public:
  DialInstance(resip::ClientRegistrationHandler*, resip::InviteSessionHandler*);
   
   void makeRegistration(const char* domain, const char* user, const char* password);
   void makeInvite(const char* sipAddress, const char* sdp);
   void shutdown();

protected:
    resip::SdpContents* build_sdp(const char *);
    
private:
    resip::SipStack* mSipStack;
    resip::StackThread* mStackThread;
    resip::DialogUsageManager* mDum;
    resip::DumThread* mDumThread;
    resip::ClientRegistrationHandler* mRegHandler;
	  resip::InviteSessionHandler* mMsgHandler;
};

#endif

