//
//  AppDelegate.m
//  IosSipRtcDialer
//
//  Created by dbezrukov on 2/11/14.
//
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@property(nonatomic, strong) SIPClient *sipClient;

@end

@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

#pragma mark - UIApplicationDelegate methods

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
        
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.viewController =
    [[ViewController alloc] initWithNibName:@"ViewController"
                                           bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [self displayLogMessage:@"Application lost focus, connection broken."];
    [self.viewController resetUI];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
}

- (void)applicationWillTerminate:(UIApplication *)application {
}

- (void)displayLogMessage:(NSString *)message {
    NSLog(@"%@", message);
    [self.viewController displayText:message];
}

#pragma mark - SIPClientDelegate methods

- (void)onStatusUpdated:(NSString*)status {
    [self displayLogMessage:status];
}

- (void)onCallingStateChanged:(int)state {
    [self.viewController setIdle:(state == 1)];
}

#pragma mark - SIPPhoneApp method

- (void)makeRegistration:(NSString *)domain user:(NSString *)user password:(NSString *)password
{
    [self displayLogMessage:[NSString stringWithFormat:@"Registering %@ on SIP server" , user]];
    
    self.sipClient = [[SIPClient alloc] initWithDelegate:self];
    [self.sipClient makeRegistration:domain user:user password:password];
}

- (void)makeCall:(NSString *)sipAddress
{
    [self.sipClient makeCall:sipAddress];
}

- (void)hangUp
{
    [self.sipClient hangUp];
}

@end






