//
//  ViewController.m
//  IosSipRtcDialer
//
//  Created by dbezrukov on 2/11/14.
//
//

#import "ViewController.h"
#import "AppDelegate.h"


@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textField.delegate = self;
    [self.view addSubview:self.namesMenu];
}

- (void)displayText:(NSString *)text {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSString *output =
        [NSString stringWithFormat:@"%@\n%@", self.textOutput.text, text];
        self.textOutput.text = output;
    });
}

- (void)setIdle:(BOOL)idle {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (idle) {
            [self resetUI];
        } else {
            [self inCallUI];
        }
    });
}

- (void)resetUI {
    self.registerView.hidden = NO;
    self.callView.hidden = NO;
    self.namesMenu.hidden = YES;
    self.textOutput.hidden = YES;
    self.textOutput.text = nil;
    self.endButton.hidden = YES;
}

- (void)inCallUI {
    self.registerView.hidden = YES;
    self.callView.hidden = YES;
    self.textOutput.hidden = NO;
    self.endButton.hidden = NO;
}

- (IBAction)callButtonTapped:(UIButton *)sender
{
    NSString *sipAddress = self.textField.text;
    if ([sipAddress length] == 0) {
        return;
    }
    
    AppDelegate<SIPPhoneApp> *app = (AppDelegate<SIPPhoneApp> *)[[UIApplication sharedApplication] delegate];
    [app makeCall:sipAddress];
}

- (IBAction)dropButtonTapped:(UIButton *)sender
{
    AppDelegate<SIPPhoneApp> *app = (AppDelegate<SIPPhoneApp> *)[[UIApplication sharedApplication] delegate];
    [app hangUp];
}

- (IBAction)iamMenuShow:(UIButton *)sender
{
    if (sender.tag == 0) {
        sender.tag = 1;
        CGRect registerFrame = [self.registerView frame];
        CGRect namesFrame = [self.namesMenu frame];
        namesFrame.origin.y = registerFrame.origin.y + registerFrame.size.height;
        self.namesMenu.frame = namesFrame;
        
        self.namesMenu.hidden = NO;
        self.callView.hidden = YES;
        [sender setTitle:@"▲" forState:UIControlStateNormal];
    } else {
        sender.tag = 0;
        self.namesMenu.hidden = YES;
        self.callView.hidden = NO;
        [sender setTitle:@"▼" forState:UIControlStateNormal];
    }
}

- (IBAction)contactMenuShow:(UIButton *)sender
{
    if (sender.tag == 0) {
        sender.tag = 1;
        CGRect callFrame = [self.callView frame];
        CGRect selectorFrame = [self.contactSelectButton frame];
        CGRect namesFrame = [self.namesMenu frame];
        namesFrame.origin.y = callFrame.origin.y + selectorFrame.origin.y + selectorFrame.size.height;
        self.namesMenu.frame = namesFrame;
        
        self.textField.hidden = YES;
        self.callButton.hidden = YES;
        self.namesMenu.hidden = NO;
        [sender setTitle:@"▲" forState:UIControlStateNormal];
    } else {
        sender.tag = 0;
        self.namesMenu.hidden = YES;
        self.textField.hidden = NO;
        self.callButton.hidden = NO;
        [sender setTitle:@"▼" forState:UIControlStateNormal];
    }
}

- (IBAction)menuSelectionMade:(UIButton *)sender
{
    // contact select
    bool contactSelection = ( self.contactSelectButton.tag == 1);
    
    NSString* userName = sender.titleLabel.text;
    if ([userName isEqualToString:@"Asterisk Sound Test"]) {
        userName = @"001111";
    }
    
    if (contactSelection) {
        self.contactText.text = sender.titleLabel.text;
        [self.contactSelectButton setTitle:@"▼" forState:UIControlStateNormal];
        self.contactSelectButton.tag = 0;
        [self.textField setText: [[NSString stringWithFormat:@"sip:%@@54.84.131.186",
                                   userName] lowercaseString]];
        self.textField.hidden = NO;
        self.callButton.hidden = NO;
    } else {
        self.iamText.text = sender.titleLabel.text;
        [self.iamSelectButton setTitle:@"[*]" forState:UIControlStateNormal];
        [self.iamSelectButton setEnabled:NO];
        
        self.iamSelectButton.tag = 0;
        self.callView.hidden = NO;
        
        // register
        AppDelegate<SIPPhoneApp> *app = (AppDelegate<SIPPhoneApp> *)[[UIApplication sharedApplication] delegate];
        [app makeRegistration:@"54.84.131.186" user:[userName lowercaseString] password:@"111111"];
    }
    self.namesMenu.hidden = YES;
}
         
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

@end
