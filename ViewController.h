//
//  ViewController.h
//  IosSipRtcDialer
//
//  Created by dbezrukov on 2/11/14.
//
//

#import <UIKit/UIKit.h>
#import <UIKit/UIPickerView.h>

@interface ViewController : UIViewController<UITextFieldDelegate, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITextView *textOutput;

@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *endButton;

@property (nonatomic, strong) IBOutlet UIView *registerView;
@property (nonatomic, strong) IBOutlet UIView *callView;

@property (nonatomic, strong) IBOutlet UILabel *iamText;
@property (nonatomic, strong) IBOutlet UIButton *iamSelectButton;

@property (nonatomic, strong) IBOutlet UILabel *contactText;
@property (nonatomic, strong) IBOutlet UIButton *contactSelectButton;

@property (nonatomic, strong) IBOutlet UIView *namesMenu;
@property (nonatomic, strong) IBOutlet UIView *contactsMenu;

- (IBAction)iamMenuShow:(UIButton *)sender;
- (IBAction)contactMenuShow:(UIButton *)sender;

- (IBAction)menuSelectionMade:(UIButton *)sender;

- (IBAction)callButtonTapped:(UIButton *)sender;
- (IBAction)dropButtonTapped:(UIButton *)sender;

- (void)displayText:(NSString *)text;
- (void)setIdle:(BOOL)text;
- (void)resetUI;

@end
