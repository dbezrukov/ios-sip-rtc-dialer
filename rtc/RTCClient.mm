#import "RTCClient.h"

#import <dispatch/dispatch.h>

#import "RTCICEServer.h"

// web rtc headers
#import "RTCICECandidate.h"
#import "RTCICEServer.h"
#import "RTCMediaConstraints.h"
#import "RTCMediaStream.h"
#import "RTCPair.h"
#import "RTCPeerConnection.h"
#import "RTCPeerConnectionDelegate.h"
#import "RTCPeerConnectionFactory.h"
#import "RTCSessionDescription.h"

@interface PCObserver : NSObject<RTCPeerConnectionDelegate>

- (id)initWithDelegate:(id<PCStateDelegate>)delegate;

@end

@implementation PCObserver {
    id<PCStateDelegate> _delegate;
}

- (id)initWithDelegate:(id<PCStateDelegate>)delegate {
    if (self = [super init]) {
        _delegate = delegate;
    }
    return self;
}

- (void)peerConnectionOnError:(RTCPeerConnection *)peerConnection {
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
 signalingStateChanged:(RTCSignalingState)stateChanged {
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
           addedStream:(RTCMediaStream *)stream {
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
         removedStream:(RTCMediaStream *)stream {
}

- (void)
peerConnectionOnRenegotiationNeeded:(RTCPeerConnection *)peerConnection {
    NSLog(@"PCO onRenegotiationNeeded.");
    // TODO(hughv): Handle this.
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
       gotICECandidate:(RTCICECandidate *)candidate {
    
    NSLog(@"PCO onICECandidate.\n  Mid[%@] Index[%d] Sdp[%@]",
          candidate.sdpMid,
          candidate.sdpMLineIndex,
          candidate.sdp);
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
   iceGatheringChanged:(RTCICEGatheringState)newState {
    
    if (newState == 2) {
        [_delegate gatheringComplete];
    }
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
  iceConnectionChanged:(RTCICEConnectionState)newState {
}

@end

@interface RTCClient ()

@property(nonatomic, strong) PCObserver *pcObserver;
@property(nonatomic, strong) RTCPeerConnection *peerConnection;
@property(nonatomic, strong) RTCPeerConnectionFactory *peerConnectionFactory;
@property(nonatomic, strong) NSMutableArray *ICEServers;
@property(nonatomic, strong) RTCMediaConstraints *mediaConstraints;
@property(nonatomic, assign) BOOL initiator;
@property(nonatomic, strong) dispatch_queue_t backgroundQueue;
@property(nonatomic, strong) id<RTCClientDelegate> rtcClientDelegate;

@end

@implementation RTCClient


- (id)init {
    if (self = [super init]) {
        [RTCPeerConnectionFactory initializeSSL];
        
        _backgroundQueue = dispatch_queue_create("RTCBackgroundQueue", NULL);
        
        // Defining media constraints
        RTCPair *audio = [[RTCPair alloc] initWithKey:@"OfferToReceiveAudio" value:@"true"];
        RTCPair *video = [[RTCPair alloc] initWithKey:@"OfferToReceiveVideo" value:@"false"];
        NSArray *mandatory = @[ audio, video ];
        self.mediaConstraints = [[RTCMediaConstraints alloc] initWithMandatoryConstraints:mandatory optionalConstraints:nil];
        
        // Defining ICE and TURN servers
        RTCICEServer *ICEServer =
        [[RTCICEServer alloc] initWithURI:[NSURL URLWithString:@"turn:54.215.136.182:3478"]
                                 username:@"testbed"
                                 password:@"21345"];
        [self.ICEServers addObject:ICEServer];
    }
    return self;
}

- (void)dealloc {
    self.rtcClientDelegate = nil;
    self.pcObserver = nil;
    
    [RTCPeerConnectionFactory deinitializeSSL];
}

- (void)createPeerConnection:(id<RTCClientDelegate>)delegate initiator:(BOOL)initiator {
    NSLog(@"RtcClient::createPeerConnection");
    
    self.rtcClientDelegate = delegate;
    self.initiator = initiator;
    
    self.peerConnectionFactory = [[RTCPeerConnectionFactory alloc] init];
    self.pcObserver = [[PCObserver alloc] initWithDelegate:self];
    self.peerConnection = [self.peerConnectionFactory peerConnectionWithICEServers:self.ICEServers
                                                                       constraints:self.mediaConstraints delegate:self.pcObserver];
    
    RTCMediaStream *lms = [self.peerConnectionFactory mediaStreamWithLabel:@"ARDAMS"];
    [lms addAudioTrack:[self.peerConnectionFactory audioTrackWithID:@"ARDAMSa0"]];
    [self.peerConnection addStream:lms constraints:self.mediaConstraints];
    
    [self.rtcClientDelegate onPeerConnectionCreated];
}


- (void)createOffer {
    NSLog(@"RtcClient::createOffer");
    [self.peerConnection createOfferWithDelegate:self constraints:self.mediaConstraints];
}

- (void)receiveRemoteDescription:(NSString*) type desc:(NSString*)desc {
    NSLog(@"RTCClient::receiveRemoteDescription,  type %@, sdp %@",  type, desc);
    
    if ([type isEqualToString:@"answer"] || [type isEqualToString:@"offer"]) {
        
        RTCSessionDescription *sdp = [[RTCSessionDescription alloc]
                                      initWithType:type
                                      sdp:[self preferCodec:desc]];
                                      //sdp:[self preferCodec:[self tuneG722:desc base:8000]]];
        [self.peerConnection setRemoteDescriptionWithDelegate:self sessionDescription:sdp];
    }
}

- (void)closePeerConnection {
    [self.peerConnection close];
    self.peerConnection = nil;
    self.peerConnectionFactory = nil;
}

#pragma mark - PCStateDelegate method

- (void)gatheringComplete {
    NSLog(@"RTCClient::gatheringComplete");
    
    // get local description and send the offer
    RTCSessionDescription* sdp = [self.peerConnection localDescription];
    
    if (self.initiator) {
        [self.rtcClientDelegate onOffer:sdp.description];//[self tuneG722:sdp.description base:16000]];
    } else {
        [self.rtcClientDelegate onAnswer:sdp.description];//[self tuneG722:sdp.description base:16000]];
    }
}

#pragma mark - RTCSessionDescriptonDelegate

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didCreateSessionDescription:(RTCSessionDescription *)origSdp
                 error:(NSError *)error {
    if (error) {
        NSLog(@"RtcClient::didCreateSessionDescription with error: %@", error.description);
        NSAssert(NO, error.description);
        return;
    }
    
    NSLog(@"RtcClient::didCreateSessionDescription, setting local desc, type: %@", origSdp.type);
    
    RTCSessionDescription* sdp = [[RTCSessionDescription alloc]
                                  initWithType:origSdp.type sdp:[self preferCodec:origSdp.description]];
    
    [self.peerConnection setLocalDescriptionWithDelegate:self sessionDescription:sdp];
}

- (void)peerConnection:(RTCPeerConnection *)peerConnection
    didSetSessionDescriptionWithError:(NSError *)error {
    if (error) {
        NSLog(@"RtcClient::didSetSessionDescriptionWithError with error: %@", error.description);
        [self closePeerConnection];
        NSAssert(NO, error.description);
        return;
    }
    
    if (self.initiator) {
        NSLog(@"RtcClient::didSetSessionDescription, skip for initiator");
    } else {
        RTCSessionDescription* sdp = [self.peerConnection localDescription];
        if (sdp == NULL) {
            NSLog(@"RtcClient::didSetSessionDescription, creating answer");
            [self.peerConnection createAnswerWithDelegate:self constraints:self.mediaConstraints];
        } else {
            NSLog(@"RtcClient::didSetSessionDescription, no sdp, skipping");
        }
    }
}

// Match |pattern| to |string| and return the first group of the first
// match, or nil if no match was found.
- (NSString *)firstMatch:(NSRegularExpression *)pattern
              withString:(NSString *)string {
    NSTextCheckingResult* result =
    [pattern firstMatchInString:string
                        options:0
                          range:NSMakeRange(0, [string length])];
    if (!result)
        return nil;
    return [string substringWithRange:[result rangeAtIndex:1]];
}

// Mangle |origSDP| to prefer the ISAC/16k audio codec.
- (NSString *)preferCodec:(NSString *)origSDP {
    int mLineIndex = -1;
    NSString* isac16kRtpMap = nil;
    NSArray* lines = [origSDP componentsSeparatedByString:@"\n"];
    NSRegularExpression* isac16kRegex = [NSRegularExpression
                                         regularExpressionWithPattern:@"^a=rtpmap:(\\d+) G722/16000[\r]?$"
                                         options:0
                                         error:nil];
    for (int i = 0;
         (i < [lines count]) && (mLineIndex == -1 || isac16kRtpMap == nil);
         ++i) {
        NSString* line = [lines objectAtIndex:i];
        if ([line hasPrefix:@"m=audio "]) {
            mLineIndex = i;
            continue;
        }
        isac16kRtpMap = [self firstMatch:isac16kRegex withString:line];
    }
    if (mLineIndex == -1) {
        NSLog(@"No m=audio line, so can't prefer iSAC");
        return origSDP;
    }
    if (isac16kRtpMap == nil) {
        NSLog(@"No ISAC/16000 line, so can't prefer iSAC");
        return origSDP;
    }
    NSArray* origMLineParts =
    [[lines objectAtIndex:mLineIndex] componentsSeparatedByString:@" "];
    NSMutableArray* newMLine =
    [NSMutableArray arrayWithCapacity:[origMLineParts count]];
    int origPartIndex = 0;
    // Format is: m=<media> <port> <proto> <fmt> ...
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:isac16kRtpMap];
    for (; origPartIndex < [origMLineParts count]; ++origPartIndex) {
        if ([isac16kRtpMap compare:[origMLineParts objectAtIndex:origPartIndex]]
            != NSOrderedSame) {
            [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex]];
        }
    }
    NSMutableArray* newLines = [NSMutableArray arrayWithCapacity:[lines count]];
    [newLines addObjectsFromArray:lines];
    [newLines replaceObjectAtIndex:mLineIndex
                        withObject:[newMLine componentsJoinedByString:@" "]];
    return [newLines componentsJoinedByString:@"\n"];
}

- (NSString*) tuneG722:(NSString*)sdp base:(int)base {
    return sdp;
    
    NSLog(@"--- tuneG77 original: %@", sdp);
    
    NSArray* lines = [sdp componentsSeparatedByString:@"\n"];
    
    int G722LineIndex = -1;
    
    NSString* pat = @"^a=rtpmap:(\\d+) G722/16000[\r]?$";
    NSString* newBase = @"8000";
    if (base == 8000) {
        pat = @"^a=rtpmap:(\\d+) G722/8000[\r]?$";
        newBase = @"16000";
    }
    
    NSString* G722RtpMap = nil;
    
    NSRegularExpression* G722Regex = [NSRegularExpression
                                         regularExpressionWithPattern:pat
                                         options:0
                                         error:nil];
    
    for (int i = 0;
         (i < [lines count]) && (G722LineIndex == -1 || G722RtpMap == nil);
         ++i) {
        NSString* line = [lines objectAtIndex:i];
        if ([line hasPrefix:@"m=audio "]) {
            G722LineIndex = i;
            continue;
        }
        G722RtpMap = [self firstMatch:G722Regex withString:line];
    }
    
    if (G722LineIndex == -1) {
        NSLog(@"No m=audio line, so can't prefer G722");
        return sdp;
    }
    if (G722RtpMap == nil) {
        NSLog(@"No G722/x line, so can't prefer G722");
        return sdp;
    }
    
    
    NSArray* origMLineParts =
        [[lines objectAtIndex:G722LineIndex] componentsSeparatedByString:@"/"];
    
    NSMutableArray* newMLine =
        [NSMutableArray arrayWithCapacity:[origMLineParts count]];
    
    int origPartIndex = 0;
    // Format is: m=<media> <port> <proto> <fmt> ...
    [newMLine addObject:[origMLineParts objectAtIndex:origPartIndex++]];
    [newMLine addObject:@"/"];
    [newMLine addObject:newBase];
    [newMLine addObject:@"\r"];
    
    NSMutableArray* newLines = [NSMutableArray arrayWithCapacity:[lines count]];
    [newLines addObjectsFromArray:lines];
    [newLines replaceObjectAtIndex:G722LineIndex
                        withObject:[newMLine componentsJoinedByString:@" "]];
    
    NSLog(@"--- tuneG77 result: %@", [newLines componentsJoinedByString:@"\n"]);
    
    return [newLines componentsJoinedByString:@"\n"];
}

@end


