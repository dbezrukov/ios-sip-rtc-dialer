#import <Foundation/Foundation.h>

@protocol RTCClientDelegate<NSObject>

- (void) onOffer:(NSString*) SDPOffer;
- (void) onAnswer:(NSString*) SDPAnswer;
- (void) onPeerConnectionCreated;
- (void) onPeerConnectionEstablished;

@end

#import "RTCSessionDescriptonDelegate.h"
#import "SIPClient.h"

@protocol PCStateDelegate<NSObject>

- (void)gatheringComplete;

@end

@interface RTCClient : NSObject<PCStateDelegate,
                                RTCSessionDescriptonDelegate>

- (void)receiveRemoteDescription:(NSString*) type desc:(NSString*)desc;
- (void)createPeerConnection:(id<RTCClientDelegate>) delegate initiator:(BOOL)initiator;
- (void)closePeerConnection;
- (void)createOffer;

@end
